__author__ = 'phe'
__created_on__ = '8/21/14 - 2014 - 08'


import cv2
import numpy as np
import argparse

def cropping(draw, ori, edge):

    returnnya = []

    for index, cnt in enumerate(draw):

        hull = cv2.convexHull(cnt)
        hull = cv2.approxPolyDP(hull, 0.1 * cv2.arcLength(hull, True), True)
        if(len(hull)) ==4:
            draw = hull
        pts = draw.reshape(4,2)
        rect = np.zeros((4,2), dtype="float32")
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        (TL, TR, BR, BL) = rect

        src = np.atleast_3d(([TL], [TR], [BR], [BL])).astype('float32')
        w, l = (300, 300)
#     dst = np.array([
#                     [-20,-20],
#                     [l+20,-20],
#                     [l+20,w+20],
#                     [-20,w+20]], dtype="float32")
        dst = np.array([
                    [+20,+20],
                    [l-20,+20],
                    [l-20,w-20],
                    [+20,w-20]], dtype="float32")

        perspective = cv2.getPerspectiveTransform(src, dst)
        out = np.zeros(shape=(300,300), dtype="uint8")
        warp = cv2.warpPerspective(ori, perspective, (l, w), out, cv2.INTER_NEAREST)
        # cv2.imwrite('P'+str(index)+".png", warp)
        returnnya[index] = warp

    return returnnya

def blurring(imagenya):
    ori = imagenya.copy()
    imagenya = cv2.cvtColor(imagenya, cv2.COLOR_BGR2GRAY)
    thresh = cv2.bitwise_not(imagenya)

    blur = cv2.findContours(
            cv2.dilate(
                cv2.Canny(
                    cv2.GaussianBlur(thresh, (9,9), 0),
                    30,70),
                None),
            126, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)

    (cnts, _) = cv2.findContours(blur.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    outline = np.zeros(ori.shape, dtype='uint8')
    cv2.drawContours(outline, cnts, -1, 255, 0)

    return outline

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--filenya", required = True,
                help = "choose the image to crop")
args = vars(ap.parse_args())

cropping(blurring(args['filenya']))